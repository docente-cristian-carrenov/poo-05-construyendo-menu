using System;

namespace Objetos
{
    public class Persona{

        //Atributos
        private string run;
        private string nombres;
        private string apellidoPaterno;
        private string apellidoMaterno;
        private string genero;
        private string fechaNacimiento;

        //Constructores
        public Persona(string run, string nombres, string apellidoPaterno, 
                        string apellidoMaterno, string genero, string fechaNacimiento)
        {
            this.run = run;
            this.nombres = nombres;
            this.apellidoPaterno = apellidoPaterno;
            this.apellidoMaterno = apellidoMaterno;
            this.genero = genero;
            this.fechaNacimiento = fechaNacimiento;            
        }

        public Persona()
        {
            this.run = "";
            this.nombres = "";
            this.apellidoPaterno = "";
            this.apellidoMaterno = "";
            this.genero = "";
            this.fechaNacimiento = "";            
        }

        // Getters & Setters
        public string Run { get; set; }
        public string Nombres { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Genero { get; set; }
        public string FechaNacimiento { get; set; }

        public void imprimir(){

            Console.WriteLine("\nDATOS PERSONALES:");
            Console.WriteLine("\nRUN: " + run);
            Console.WriteLine("\nNOMBRES: " + nombres);
            Console.WriteLine("\nAPELLIDO PATERNO: " + apellidoPaterno);
            Console.WriteLine("\nAPELLIDO MATERNO: " + apellidoMaterno);
            Console.WriteLine("\nGÉNERO: " + genero);
            Console.WriteLine("\nFECHA DE NACIMIENTO: " + fechaNacimiento);  
        }
    }

}