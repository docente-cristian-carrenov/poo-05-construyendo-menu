using System;

namespace Util
{
    public class ConsoleUtil
    {

       public static void LimpiarPantalla(){
           Console.Clear();
           System.Threading.Thread.Sleep(TimeSpan.FromSeconds(1));
       } 

       public static void Pause(int time){
           System.Threading.Thread.Sleep(TimeSpan.FromSeconds(time));
       }        
    }

}