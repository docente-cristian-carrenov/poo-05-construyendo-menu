using System;
using Util;
using Objetos;


namespace Presentacion
{
    public class Menu {
        //Atributos
        private Persona persona;

        //Constructores
        public Menu(Persona persona) {
            this.persona = persona;
        }
        
        public Menu() {
            this.persona = new Persona();
        }

        // Getters & Setters
        public string Persona { get; set; } 

        public void EjecutarMenu(){
        
            int op = -1;
            
            while(op != 3){
            
                string title = "\t\t\tMENÚ PRINCIPAL\n\n\n";
                Console.WriteLine(title);
                
                string descripcionOpciones = "1 - Crear Persona" +
                        "\n" + "2 - Listar Persona" +
                        "\n3 - Salir";
                Console.WriteLine(descripcionOpciones);
                            
                Console.WriteLine("\n\nSeleccione una opción");
                
                op = Convert.ToInt32(Console.ReadLine());
                
                switch(op){
                
                    case 1: 
                        OpcionUno();
                        ConsoleUtil.LimpiarPantalla();
                        break;
                    case 2:
                        OpcionDos();
                        ConsoleUtil.Pause(2);
                        ConsoleUtil.LimpiarPantalla();
                        break;
                        
                    case 3:
                        Console.WriteLine("PROGRAMA FINALIZADO");
                        break;
                        
                    default:
                        
                        Console.WriteLine("OPCIÓN INVÁLIDA");
                        ConsoleUtil.LimpiarPantalla();
                        break;
                    
                }                        
            }                
        }

        public void OpcionUno(){
        
            //Leer Información desde teclado
            Console.Write("\nINGRESE RUN: ");
            string run = Console.ReadLine();
            Console.Write("\nINGRESE NOMBRES: ");
            string nombres = Console.ReadLine();
            Console.Write("\nINGRESE APELLIDO PATERNO: ");
            string apellidoPaterno = Console.ReadLine();
            Console.Write("\nINGRESE APELLIDO MATERNO: ");
            string apellidoMaterno = Console.ReadLine();
            Console.Write("\nINGRESE GENERO: ");
            string genero = Console.ReadLine();
            Console.Write("\nINGRESE FECHA DE NACIMIENTO: ");
            string fechaNacimiento = Console.ReadLine();        
            
            persona = new Persona(run, nombres, apellidoPaterno, 
                    apellidoMaterno, genero, fechaNacimiento);
        }    

        public void OpcionDos(){
        
            persona.imprimir();
        }  
    }  
}