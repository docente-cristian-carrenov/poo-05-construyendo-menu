/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.tutorial.presentacion;

import cl.tutorial.objetos.Persona;
import cl.tutorial.utilitario.ConsoleUtil;
import java.util.Scanner;

/**
 *
 * @author ccarrenov
 */
public class Menu {
    
    //Atributos
    private Persona persona;
    
    //Constructores
    public Menu(Persona persona) {
        this.persona = persona;
    }
    
    public Menu() {
        this.persona = new Persona();
    }
    
    //Getters & Setters

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
    /**
     * Méto encargado de generar menú con ciclo definido según opción de salida
     */
    public void ejecutarMenu(){
    
        int op = -1;
        
        while(op != 3){
        
            String title = "\t\t\tMENÚ PRINCIPAL\n\n\n";
            System.out.println(title);
            
            String descripcionOpciones = "1 - Crear Persona" +
                    "\n" + "2 - Listar Persona" +
                    "\n3 - Salir";
            System.out.println(descripcionOpciones);
            
            Scanner sc = new Scanner(System.in);
            
            System.out.println("\n\nSeleccione una opción");
            
            op = sc.nextInt();
            
            switch(op){
            
                case 1: 
                    opcionUno();
                    ConsoleUtil.limpiarPantalla();
                    break;
                case 2:
                    opcionDos();
                    ConsoleUtil.pause(2500);
                    ConsoleUtil.limpiarPantalla();
                    break;
                    
                case 3:
                    System.out.println("PROGRAMA FINALIZADO");
                    break;
                    
                default:
                    
                    System.out.println("OPCIÓN INVÁLIDA");
                    ConsoleUtil.limpiarPantalla();
                    break;
                
            }                        
        }                
    }
    
    /**
     * Método encargado de realizar lógica para la opción uno (Crear Persona)
     */
    public void opcionUno(){
    
        //Leer Información desde teclado
        Scanner sc = new Scanner(System.in);
        System.out.print("\nINGRESE RUN: ");
        String run = sc.nextLine();
        System.out.print("\nINGRESE NOMBRES: ");
        String nombres = sc.nextLine();
        System.out.print("\nINGRESE APELLIDO PATERNO: ");
        String apellidoPaterno = sc.nextLine();
        System.out.print("\nINGRESE APELLIDO MATERNO: ");
        String apellidoMaterno = sc.nextLine();
        System.out.print("\nINGRESE GENERO: ");
        String genero = sc.nextLine();
        System.out.print("\nINGRESE FECHA DE NACIMIENTO: ");
        String fechaNacimiento = sc.nextLine();        
        
        persona = new Persona(run, nombres, apellidoPaterno, 
                apellidoMaterno, genero, fechaNacimiento);
    }
    
    /**
     * Método encargado de realizar lógica para la opción dos (listar persona)
     */
    public void opcionDos(){
    
        persona.imprimir();
    }
    
}
