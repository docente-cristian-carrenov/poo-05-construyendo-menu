/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.tutorial.utilitario;

import java.awt.Robot;
import java.awt.event.KeyEvent;

/**
 *
 * @author ccarrenov
 */
public class ConsoleUtil {
    
    /**
     * Método encargado de limpiar pantalla
     */
    public static void limpiarPantalla(){
    
        try{
        
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_L);
            robot.delay(500);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            robot.keyRelease(KeyEvent.VK_L);   
            robot.delay(500);
        }catch(Exception ex){
            
            System.out.println(ex.getMessage());
        }
    }
    
    /**
     * Método encargado de esperar n milisegundos
     * @param time tiempo de espera en milisegundos
     */
    public static void pause(int time){
        try{
            Robot robot = new Robot();              
            robot.delay(time);            
        }catch(Exception ex){
            
            System.out.println(ex.getMessage());
        }                
            
    }
}
